package com.project_one.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project_one.model.Reimbursment;
import com.project_one.model.Users;
import com.project_one.service.ReimbursementServiceImpl;
import com.project_one.service.ReimbursmentService;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

/**
 * Servlet implementation class ReqReimbServlet
 */
public class ReqReimbServlet extends HttpServlet {
	
	UserService us = new UserServiceImpl();
	ReimbursmentService reimServ = new ReimbursementServiceImpl();
	final Logger logger = LogManager.getLogger(ReqReimbServlet.class);

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher(RequestHelper.process(request)).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String mysess = (String) session.getAttribute("loguser");
		Users user = us.getUserInfoByName(mysess);
		
		int id = new Random().nextInt(90000) + 10000;
		int amount = Integer.parseInt(request.getParameter("amount"));
		String description = request.getParameter("description");
		int type = Integer.parseInt(request.getParameter("select-form"));
		
		reimServ.createReimbursement(id, amount, LocalDate.now(), 
				null, description,null, user.getUser_id(), 
				0, 1, type);
		logger.debug("Reimbursment has been created");
		request.getRequestDispatcher("/main.html").forward(request, response);
	}

}
