package com.project_one.servlet;

import javax.servlet.http.HttpServletRequest;

import com.project_one.controller.CreateReimController;
import com.project_one.controller.HistoryController;
import com.project_one.controller.LoginController;
import com.project_one.controller.LogoutController;
import com.project_one.controller.MainController;
import com.project_one.controller.ManageController;

public class RequestHelper {
	
	public static String process(HttpServletRequest req) {
		
		switch(req.getRequestURI()) {
		
		case "/BananyaReimbursment/login":
			return LoginController.login(req);
		
		case "/BananyaReimbursment/logout":
			return LogoutController.logout(req);
			
		case "/BananyaReimbursment/main":
			return MainController.mainc(req);
			
		case "/BananyaReimbursment/history":
			 return HistoryController.history(req);
		
		case "/BananyaReimbursment/reimb":
			 return CreateReimController.createreim(req);
			 
		case "/BananyaReimbursment/manage":
			 return ManageController.manage(req);
			
		default:
			return "/BananyaReimbursment/404.html";
		
		}
	}
}
