package com.project_one.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project_one.model.Reimbursment;
import com.project_one.service.ReimbursementServiceImpl;
import com.project_one.service.ReimbursmentService;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

/**
 * Servlet implementation class PendingServlet
 */
public class PendingServlet extends HttpServlet {
	
	UserService us = new UserServiceImpl();
	ReimbursmentService rs = new ReimbursementServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mysess = (String) session.getAttribute("loguser");
		List<Reimbursment> reims = rs.allReimbursement(1);
		
		PrintWriter printer = response.getWriter();	
		response.setContentType("application/json");
		printer.write(new ObjectMapper().writeValueAsString(reims));
	}

}
