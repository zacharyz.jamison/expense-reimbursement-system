package com.project_one.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project_one.model.Reimbursment;
import com.project_one.model.Users;
import com.project_one.service.ReimbursementServiceImpl;
import com.project_one.service.ReimbursmentService;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

/**
 * Servlet implementation class JsonServlet
 */
public class ReimbursementServlet extends HttpServlet {
	
	ReimbursmentService reimServ = new ReimbursementServiceImpl();
	UserService us = new UserServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		/*
		String info = request.getPathInfo();
		String[] parts = info.split("/");
		
		System.out.print(parts.length);
		if(parts.length > 1) {
			Reimbursment r = reimServ.getReimbursement(Integer.parseInt(parts[1]));
			PrintWriter printer = response.getWriter();	
			response.setContentType("application/json");
			printer.write(new ObjectMapper().writeValueAsString(r));
		} else {
			List<Reimbursment> reim = reimServ.allReimbursement(1);
			PrintWriter printer = response.getWriter();	
			response.setContentType("application/json");
			for(Reimbursment i: reim) {
				printer.write(new ObjectMapper().writeValueAsString(i));
			}
		}
		*/
		
		HttpSession session = request.getSession();
		String mysess = (String) session.getAttribute("loguser");
		Users user = us.getUserInfoByName(mysess);
		
		List<Reimbursment> reim = reimServ.userReimbursement(user.getUser_id());
		PrintWriter printer = response.getWriter();	
		response.setContentType("application/json");
		printer.write(new ObjectMapper().writeValueAsString(reim));
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
