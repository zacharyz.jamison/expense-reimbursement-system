package com.project_one.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.project_one.model.Users;
import com.project_one.service.ReimbursementServiceImpl;
import com.project_one.service.ReimbursmentService;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

/**
 * Servlet implementation class DeclineServlet
 */
public class DeclineServlet extends HttpServlet {
	UserService us = new UserServiceImpl();
	ReimbursmentService reimServ = new ReimbursementServiceImpl();
	final Logger logger = LogManager.getLogger(DeclineServlet.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String info = request.getPathInfo();
		String[] parts = info.split("/");
		
		HttpSession session = request.getSession();
		String mysess = (String) session.getAttribute("loguser");
		Users user = us.getUserInfoByName(mysess);
		
		reimServ.declinePending(Integer.parseInt(parts[1]), user.getUser_id());
		logger.debug("Reimbursment has been Declined");
		response.sendRedirect("/BananyaReimbursment/manage.html");
	
	}

}
