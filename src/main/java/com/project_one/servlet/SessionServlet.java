package com.project_one.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project_one.model.Reimbursment;
import com.project_one.model.Users;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

/**
 * Servlet implementation class SessionServlet
 */
public class SessionServlet extends HttpServlet {
	
	UserService us = new UserServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		String mysess = (String) session.getAttribute("loguser");
		Users user = us.getUserInfoByName(mysess);
		
		PrintWriter printer = response.getWriter();	
		response.setContentType("application/json");
		printer.write(new ObjectMapper().writeValueAsString(user));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
