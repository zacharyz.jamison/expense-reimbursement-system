package com.project_one.model;

import java.time.LocalDate;

public class Reimbursment {

	private int reimb_id;
	private int reimb_amount;
	private LocalDate reimb_submitted;
	private LocalDate reimb_resolved;
	private String description;
	private String reimb_receipt_url;
	private int reimb_author;
	private int reimb_resolver;
	private int reimb_status_id;
	private int reimb_type_id;
	
	public Reimbursment() {
		super();
	}

	public Reimbursment(int reimb_id, int reimb_amount, LocalDate reimb_submitted, LocalDate reimb_resolved, String description,
			String reimb_receipt_url,int reimb_author, int reimb_resolver, int reimb_status_id, int reimb_type_id) {
		super();
		this.reimb_id = reimb_id;
		this.reimb_amount = reimb_amount;
		this.reimb_submitted = reimb_submitted;
		this.reimb_resolved = reimb_resolved;
		this.description = description;
		this.reimb_receipt_url = reimb_receipt_url;
		this.reimb_author = reimb_author;
		this.reimb_resolver = reimb_resolver;
		this.reimb_status_id = reimb_status_id;
		this.reimb_type_id = reimb_type_id;
	}

	public int getReimb_id() {
		return reimb_id;
	}

	public void setReimb_id(int reimb_id) {
		this.reimb_id = reimb_id;
	}

	public int getReimb_amount() {
		return reimb_amount;
	}

	public void setReimb_amount(int reimb_amount) {
		this.reimb_amount = reimb_amount;
	}

	public LocalDate getReimb_submitted() {
		return reimb_submitted;
	}

	public void setReimb_submitted(LocalDate reimb_submitted) {
		this.reimb_submitted = reimb_submitted;
	}

	public LocalDate getReimb_resolved() {
		return reimb_resolved;
	}

	public void setReimb_resolved(LocalDate reimb_resolved) {
		this.reimb_resolved = reimb_resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReimb_receipt_url() {
		return reimb_receipt_url;
	}

	public void setReimb_receipt_url(String reimb_receipt_url) {
		this.reimb_receipt_url = reimb_receipt_url;
	}

	public int getReimb_author() {
		return reimb_author;
	}

	public void setReimb_author(int reimb_author) {
		this.reimb_author = reimb_author;
	}

	public int getReimb_resolver() {
		return reimb_resolver;
	}

	public void setReimb_resolver(int reimb_resolver) {
		this.reimb_resolver = reimb_resolver;
	}

	public int getReimb_status_id() {
		return reimb_status_id;
	}

	public void setReimb_status_id(int reimb_status_id) {
		this.reimb_status_id = reimb_status_id;
	}

	public int getReimb_type_id() {
		return reimb_type_id;
	}

	public void setReimb_type_id(int reimb_type_id) {
		this.reimb_type_id = reimb_type_id;
	}

	@Override
	public String toString() {
		return "Reimbursment [reimb_id=" + reimb_id + ", reimb_amount=" + reimb_amount + ", reimb_submitted="
				+ reimb_submitted + ", reimb_resolved=" + reimb_resolved + ", description=" + description
				+ ", reimb_receipt_url=" + reimb_receipt_url + ", reimb_author=" + reimb_author + ", reimb_resolver="
				+ reimb_resolver + ", reimb_status_id=" + reimb_status_id + ", reimb_type_id=" + reimb_type_id + "]";
	}

	
		
}
