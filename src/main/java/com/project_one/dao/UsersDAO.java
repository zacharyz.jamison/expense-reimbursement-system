package com.project_one.dao;

import com.project_one.model.Users;

public interface UsersDAO {

	/*
	 * Users CRUD Methods
	 */
	
	int createUser(int user_id, String username, String password, String firstName, String lastName, String email,
			int role_id);
	
	boolean getRole(int user_id);
	boolean getRole(String username);
	boolean userlogin(String username, String password);
	Users getInfo(int user_id);
	Users getInfo(String username);
}
