package com.project_one.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.project_one.model.Reimbursment;

public interface ReimbursementDAO {
	
	int create_reim(int reimb_id, int reimb_amount, LocalDate reimb_submitted, LocalDate reimb_resolved, String description,
			String reimb_receipt_url,int reimb_author, int reimb_resolver, int reimb_status_id, int reimb_type_id);
	int set_status(int reimb_id, int status);
	
	int reim_status(int reimb_id);
	String getDescription(int reimb_id);
	Reimbursment getReim(int reimb_id);
	List<Reimbursment> list_reim(int reimb_status_id);
	List<Reimbursment> user_reim(int author);
	void approve(int reimb_id, int resolver);
	void denied(int reimb_id, int resolver);
	

}
