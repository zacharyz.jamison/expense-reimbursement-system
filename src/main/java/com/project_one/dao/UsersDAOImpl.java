package com.project_one.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.project_one.model.Users;

public class UsersDAOImpl implements UsersDAO {
	
	String url = System.getenv("URL");
	String db_username = System.getenv("USERNAME");
	String db_password = System.getenv("PASSWORD");
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }

	@Override
	public int createUser(int user_id, String username, String password, String firstName, String lastName,
			String email, int role_id) {
		int rs = 0;
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "insert into ers_users values (?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user_id);
			ps.setString(2, username);
			ps.setString(3,password);
			ps.setString(4, firstName);
			ps.setString(5, lastName);
			ps.setString(6, email);
			ps.setInt(7, role_id);
			rs = ps.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}

	@Override
	public boolean getRole(int user_id) {
		// TODO Auto-generated method stub
		boolean role = false;
		
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "select role_id from ers_users where ers_users_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user_id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				role = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return role;
	}

	@Override
	public boolean getRole(String username) {
		// TODO Auto-generated method stub
		boolean role = false;
		
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "select role_id from ers_users where ers_username = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				role = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return role;
	}

	@Override
	public boolean userlogin(String username, String password) {
		// TODO Auto-generated method stub
		boolean login = false;
		
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "select * from ers_users where ers_username = ? and ers_password = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				login = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return login;
	}

	@Override
	public Users getInfo(int user_id) {
		// TODO Auto-generated method stub
		Users user = new Users();
		
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "select * from ers_users where user_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, user_id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				user.setUser_id(user_id);
				user.setUsername(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFirstName(rs.getString(4));
				user.setLastName(rs.getString(5));
				user.setEmail(rs.getString(6));
				user.setRole_id(rs.getInt(7));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	@Override
	public Users getInfo(String username) {
		// TODO Auto-generated method stub
		Users user = new Users();
		
		try(Connection conn = DriverManager.getConnection(url,db_username,db_password)) {
			
			String sql = "select * from ers_users where ers_username = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				user.setUser_id(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setPassword(rs.getString(3));
				user.setFirstName(rs.getString(4));
				user.setLastName(rs.getString(5));
				user.setEmail(rs.getString(6));
				user.setRole_id(rs.getInt(7));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

}
