package com.project_one.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.project_one.model.Reimbursment;

public class ReimbursementDAOImpl implements ReimbursementDAO {
	
	String url = System.getenv("URL");
	String username = System.getenv("USERNAME");
	String password = System.getenv("PASSWORD");
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }


	@Override
	public int create_reim(int reimb_id, int reimb_amount, LocalDate reimb_submitted, LocalDate reimb_resolved,
			String description, String reimb_receipt_url, int reimb_author, int reimb_resolver, int reimb_status_id,
			int reimb_type_id) {
		int rs = 0;
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "insert into ers_reimbursement values (?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb_id);
			ps.setInt(2, reimb_amount);
			ps.setObject(3,reimb_submitted);  
			ps.setObject(4,reimb_resolved);
			ps.setString(5, description);
			ps.setString(6, reimb_receipt_url);
			ps.setInt(7, reimb_author);
			ps.setInt(8, reimb_resolver);
			ps.setInt(9, reimb_status_id);
			ps.setInt(10, reimb_type_id);
			rs = ps.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}

	@Override
	public int set_status(int reimb_id, int status) {
		int rs = 0;
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "update ers_reimbursement set reimb_type_id = ? where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, status);
			ps.setInt(2, reimb_id);
			
			rs = ps.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}

	@Override
	public int reim_status(int reimb_id) {
		
		int status = 0;
		
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "select reimb_type_id from ers_reimbursement where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb_id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				status = rs.getInt(1);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return status;
	}

	@Override
	public String getDescription(int reimb_id) {
		// TODO Auto-generated method stub
		String des = new String();
		
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "select description from ers_reimbursement where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb_id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				des = rs.getString(1);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return des;
	}

	@Override
	public Reimbursment getReim(int reimb_id) {
		// TODO Auto-generated method stub
		Reimbursment reimb = new Reimbursment();
		
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "select * from ers_reimbursement where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb_id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				reimb.setReimb_id(rs.getInt(1));
				reimb.setReimb_amount(rs.getInt(2));
				reimb.setReimb_submitted(rs.getObject(3, LocalDate.class));
				reimb.setReimb_resolved(rs.getObject(4, LocalDate.class));
				reimb.setDescription(rs.getString(5));
				reimb.setReimb_receipt_url(rs.getString(6));
				reimb.setReimb_author(rs.getInt(7));
				reimb.setReimb_resolver(rs.getInt(8));
				reimb.setReimb_status_id(rs.getInt(9));
				reimb.setReimb_type_id(rs.getInt(10));
				
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimb;
	}

	@Override
	public List<Reimbursment> list_reim(int reimb_status_id) {
		// TODO Auto-generated method stub
		List<Reimbursment> reimb = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "select * from ers_reimbursement where reimb_status_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimb_status_id);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				reimb.add(new Reimbursment(rs.getInt(1),rs.getInt(2),rs.getObject(3, LocalDate.class),
				rs.getObject(4, LocalDate.class),rs.getString(5),rs.getString(6),rs.getInt(7),
				rs.getInt(8),rs.getInt(9),rs.getInt(10)));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimb;
	}

	@Override
	public void approve(int reimb_id, int resolver) {
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "update ers_reimbursement set reimb_status_id = ?, reimb_resolver = ?, "
					+ "reimb_resolved = ?  where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, 2);
			ps.setInt(2, resolver);
			ps.setObject(3, LocalDate.now());
			ps.setInt(4, reimb_id);
			
			ps.executeUpdate();
			
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void denied(int reimb_id,int resolver) {
		// TODO Auto-generated method stub
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "update ers_reimbursement set reimb_status_id = ?, reimb_resolver = ?, "
					+ "reimb_resolved = ?  where reimb_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, 3);
			ps.setInt(2, resolver);
			ps.setObject(3, LocalDate.now());
			ps.setInt(4, reimb_id);
			
			ps.executeUpdate();
			
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Reimbursment> user_reim(int author) {
		// TODO Auto-generated method stub
		List<Reimbursment> reimb = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url,username,password)) {
			
			String sql = "select * from ers_reimbursement where reimb_author = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, author);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				reimb.add(new Reimbursment(rs.getInt(1),rs.getInt(2),rs.getObject(3, LocalDate.class),
				rs.getObject(4, LocalDate.class),rs.getString(5),rs.getString(6),rs.getInt(7),
				rs.getInt(8),rs.getInt(9),rs.getInt(10)));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimb;
	}
	
	

}
