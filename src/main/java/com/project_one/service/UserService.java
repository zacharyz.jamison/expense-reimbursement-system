package com.project_one.service;

import com.project_one.model.Reimbursment;
import com.project_one.model.Users;

public interface UserService {
	
	boolean checkUser(String username, String password);
	Users getUserInfo(int user_id);
	Users getUserInfoByName(String username);
	int approveReim(Reimbursment r);
	int declineReim(Reimbursment r);
	
	
}
