package com.project_one.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.project_one.model.Reimbursment;

public interface ReimbursmentService {
	
	int createReimbursement(int reimb_id, int reimb_amount, LocalDate reimb_submitted, LocalDate reimb_resolved, String description,
			String reimb_receipt_url,int reimb_author, int reimb_resolver, int reimb_status_id, int reimb_type_id);
	Reimbursment getReimbursement(int reimb_id);
	List<Reimbursment> allReimbursement(int status_id);
	List<Reimbursment> userReimbursement(int author);
	void approvePending(int reimb_id, int resolver);
	void declinePending(int reimb_id, int resolver);

}
