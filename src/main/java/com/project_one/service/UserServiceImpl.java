package com.project_one.service;

import com.project_one.dao.ReimbursementDAO;
import com.project_one.dao.ReimbursementDAOImpl;
import com.project_one.dao.UsersDAO;
import com.project_one.dao.UsersDAOImpl;
import com.project_one.model.Reimbursment;
import com.project_one.model.Users;

public class UserServiceImpl implements UserService {
	
	UsersDAO users = new UsersDAOImpl();
	ReimbursementDAO reim = new ReimbursementDAOImpl();

	@Override
	public boolean checkUser(String username, String password) {
		// TODO Auto-generated method stub
		boolean check = users.userlogin(username, password);
		return check;
	}

	@Override
	public Users getUserInfo(int user_id) {
		// TODO Auto-generated method stub
		Users user = users.getInfo(user_id);
		return user;
	}

	@Override
	public int approveReim(Reimbursment r) {
		// TODO Auto-generated method stub
		int status = reim.set_status(r.getReimb_id(), 2);
		return status;
	}

	@Override
	public int declineReim(Reimbursment r) {
		// TODO Auto-generated method stub
		int status = reim.set_status(r.getReimb_id(), 3);
		return status;
	}

	@Override
	public Users getUserInfoByName(String username) {
		// TODO Auto-generated method stub
		Users user = users.getInfo(username);
		return user;
	}

}
