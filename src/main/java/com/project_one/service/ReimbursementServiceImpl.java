package com.project_one.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.project_one.dao.ReimbursementDAO;
import com.project_one.dao.ReimbursementDAOImpl;
import com.project_one.model.Reimbursment;

public class ReimbursementServiceImpl implements ReimbursmentService {
	
	ReimbursementDAO reim = new ReimbursementDAOImpl();

	@Override
	public int createReimbursement(int reimb_id, int reimb_amount, LocalDate reimb_submitted, LocalDate reimb_resolved,
			String description, String reimb_receipt_url, int reimb_author, int reimb_resolver, int reimb_status_id,
			int reimb_type_id) {
		int status = reim.create_reim(reimb_id, reimb_amount, reimb_submitted, reimb_resolved, description, reimb_receipt_url, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id);
		return status;
	}

	@Override
	public Reimbursment getReimbursement(int reimb_id) {
		// TODO Auto-generated method stub
		Reimbursment r = reim.getReim(reimb_id);
		return r;
	}
	
	@Override
	public List<Reimbursment> allReimbursement(int status_id) {
		return reim.list_reim(status_id);
	}

	@Override
	public void approvePending(int reimb_id, int resolver) {
		// TODO Auto-generated method stub
		reim.approve(reimb_id, resolver);
	}

	@Override
	public void declinePending(int reimb_id, int resolver) {
		// TODO Auto-generated method stub
		reim.denied(reimb_id, resolver);
	}

	@Override
	public List<Reimbursment> userReimbursement(int author) {
		// TODO Auto-generated method stub
		return reim.user_reim(author);
	}

	

}
