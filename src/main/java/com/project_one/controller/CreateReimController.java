package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CreateReimController {
	
	public static String createreim(HttpServletRequest req) {
		
		HttpSession session= req.getSession();  
		if(session.getAttribute("loguser") != null) {
			return "/reimb.html";
		} else {
			return "/";
		}
		
        
	}

}
