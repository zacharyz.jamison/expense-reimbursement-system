package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

public class LoginController {
	
	public static String login(HttpServletRequest req) {
		
		UserService us = new UserServiceImpl();
		final Logger logger = LogManager.getLogger(LoginController.class);
		
		if(!req.getMethod().equals("POST")) {
			return "/";
		} 
		
		String user = req.getParameter("username");
		String pass = req.getParameter("password");
		
		
		if(!us.checkUser(user, pass)) {
			return "/";
			
		} else {
			logger.debug(user +" User signed in");
			req.getSession().setAttribute("loguser", user);
			req.getSession().setAttribute("logpass", pass);
			return "/main.html";
		}
	}
}
