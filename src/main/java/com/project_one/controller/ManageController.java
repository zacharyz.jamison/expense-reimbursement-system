package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.project_one.model.Users;
import com.project_one.service.UserService;
import com.project_one.service.UserServiceImpl;

public class ManageController {
	
	public static String manage(HttpServletRequest req) {
			
		UserService us = new UserServiceImpl();
		HttpSession session= req.getSession(); 
		
		if(session.getAttribute("loguser") != null) {
			Users user = us.getUserInfoByName((String) session.getAttribute("loguser"));
			if(user.getRole_id() != 2) {
				return "/main.html";
			} else {
				return "/manage.html";
			}
		} else {
			return "/";
		}
		
	        
	}
}
