package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class HistoryController {
	
	public static String history(HttpServletRequest req) {
			
		HttpSession session= req.getSession();  
		if(session.getAttribute("loguser") != null) {
			
			//logic stuff
			
			return "/history.html";
		} else {
			return "/";
		}
		
        
	}
}
