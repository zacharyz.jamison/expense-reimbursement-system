package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class MainController {
	
	public static String mainc(HttpServletRequest req) {
		
		HttpSession session= req.getSession();  
		if(session.getAttribute("loguser") != null) {	
			//logic stuff	
			return "/main.html";
		} else {
			return "/";
		}
		
        
	}
}
