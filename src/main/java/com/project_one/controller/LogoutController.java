package com.project_one.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogoutController {
		
	public static String logout(HttpServletRequest req) {
			
		HttpSession session= req.getSession();  
        session.invalidate();
        final Logger logger = LogManager.getLogger(LogoutController.class);
        logger.debug("User signed out");
        return "/";
	}
}
