"use strict";

window.onload = function() {
	let xhttp = new XMLHttpRequest();
	let url = "http://localhost:5000/BananyaReimbursment/pending";
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			let m = JSON.parse(xhttp.responseText);
			console.log(m);
			let i;
			for(i = 0; i < m.length; i++){
				let card = document.createElement('div');
				card.className = "card";
				card.style.width = "30rem";
				card.style.margin = " 0 auto";
				
				let cardBody = document.createElement('div');
				cardBody.className = "card-body";
				
				let cardTitle = document.createElement('h5');
				cardTitle.className = "card-title";
				cardTitle.innerText= "Reimb ID#:"+m[i].reimb_id;
				
				let cardSubtitle = document.createElement('h6');
				cardSubtitle.className = "card-subtitle mb-2 text-muted";
				cardSubtitle.innerText="Amount $"+ m[i].reimb_amount;
				
				let cardText = document.createElement('p');
				cardText.className = "card-text";
				cardText.innerText= m[i].description;
				
				let approve = document.createElement('form');
				approve.action = "/BananyaReimbursment/approve/"+m[i].reimb_id;
				approve.method = "post";
				let inputOne = document.createElement('input');
				inputOne.type = "submit";
				inputOne.className = "btn btn-success";
				inputOne.name = "approve";
				inputOne.value = "approve";
				approve.appendChild(inputOne);
				
				let decline = document.createElement('form');
				decline.action = "/BananyaReimbursment/decline/"+m[i].reimb_id;
				decline.method = "post";
				let inputTwo = document.createElement('input');
				inputTwo.type = "submit";
				inputTwo.className = "btn btn-danger";
				inputTwo.name = "decline";
				inputTwo.value = "decline";
				decline.appendChild(inputTwo);
				
				let br = document.createElement('br');
				
				cardBody.appendChild(cardTitle);
				cardBody.appendChild(cardSubtitle);
				cardBody.appendChild(cardText);
				cardBody.appendChild(approve);
				cardBody.appendChild(decline);
				cardBody.appendChild(br);
				
				card.appendChild(cardBody)
				
				let historyCard = document.getElementById('decision-card');
				
				historyCard.append(card);
				historyCard.append(br);
				
			} 
			
		}
	}
	
	xhttp.open("GET", url);
	
	xhttp.send();
}