"use strict";

window.onload = function() {
	let xhttp = new XMLHttpRequest();
	let url = "http://localhost:5000/BananyaReimbursment/api/reimbursment";
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			let m = JSON.parse(xhttp.responseText);
			console.log(m);
			let i;
			for(i = 0; i < m.length; i++){
				let card = document.createElement('div');
				card.className = "card";
				card.style.width = "30rem";
				card.style.margin = " 0 auto";
				
				let cardBody = document.createElement('div');
				cardBody.className = "card-body";
				
				let cardTitle = document.createElement('h5');
				cardTitle.className = "card-title";
				cardTitle.innerText= "Reimb ID#:"+m[i].reimb_id;
				
				let cardSubtitle = document.createElement('h6');
				cardSubtitle.className = "card-subtitle mb-2 text-muted";
				cardSubtitle.innerText="Amount $"+ m[i].reimb_amount;
				
				let cardText = document.createElement('p');
				cardText.className = "card-text";
				cardText.innerText= m[i].description;
				
				let linkOne = document.createElement('h6');
				linkOne.className = "card-link";
				linkOne.innerText = "Date Submitted: "+m[i].reimb_submitted.month
					+"/"+m[i].reimb_submitted.dayOfMonth+"/"+m[i].reimb_submitted.year;
				
				let linkTwo = document.createElement('h6');
				if(m[i].reimb_resolved == null) {
					linkTwo.className = "card-link";
					linkTwo.innerText = "Date Resolved: Not Resolved";
				} else {
					linkTwo.className = "card-link";
					linkTwo.innerText = "Date Resolved: "+m[i].reimb_resolved.month
						+"/"+m[i].reimb_resolved.dayOfMonth+"/"+m[i].reimb_resolved.year;
				}
				
				let status = document.createElement('h6');
				if(m[i].reimb_status_id == 2) {
					status.className = "card-link";
					status.innerText = "Status: APPROVED";
				} else if (m[i].reimb_status_id == 3) {
					status.className = "card-link";
					linkTwo.innerText = "Status: DECLINED";
				} else {
					status.className = "card-link";
					status.innerText = "Status: PENDING";
				}
				
				let br = document.createElement('br');
				
				cardBody.appendChild(cardTitle);
				cardBody.appendChild(cardSubtitle);
				cardBody.appendChild(cardText);
				cardBody.appendChild(linkOne);		
				cardBody.appendChild(linkTwo);
				cardBody.appendChild(status);
				cardBody.appendChild(br);
				
				card.appendChild(cardBody)
				
				let historyCard = document.getElementById('history-card');
				
				historyCard.append(card);
				historyCard.append(br);
				
			} 
			
		}
	}
	
	xhttp.open("GET", url);
	
	xhttp.send();
}