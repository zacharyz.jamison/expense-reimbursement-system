# Expense Reimbursment System

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

- Servlets 
- Java 
- JavaScript 
- HTML 
- CSS 
- JDBC

## Getting Started

- git clone 
- Install tomcat 9.0 in Java IDE
- have postgres installed
- create database schema from image in schema section

## Usage

![App Home Photo](Screen_Shot_2020-12-27_at_8.48.50_PM.png)
![Database Schema](Screen_Shot_2021-01-11_at_11.27.06_AM.png)
